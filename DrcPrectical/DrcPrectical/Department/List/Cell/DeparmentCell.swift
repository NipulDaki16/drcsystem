//
//  DeparmentCell.swift
//  DrcPrectical
//
//  Created by Nipul Daki on 09/03/22.
//

import UIKit

class DeparmentCell: UITableViewCell {

    @IBOutlet weak var lblDeptName: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
