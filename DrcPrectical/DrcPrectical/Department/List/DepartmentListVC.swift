//
//  DepartmentListVC.swift
//  DrcPrectical
//
//  Created by Nipul Daki on 09/03/22.
//

import UIKit

class DepartmentListVC: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupScreen()
        // Do any additional setup after loading the view.
    }
    
    func setupScreen() {
        tblView.register(cellType: DeparmentCell.self)
        tblView.delegate = self
        tblView.dataSource = self
    }
}

extension DepartmentListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if let cell = tableView.dequeueReusableCell(withIdentifier: DeparmentCell.identifier) as? DeparmentCell {

            return cell
        }
        return UITableViewCell()
    }
}
