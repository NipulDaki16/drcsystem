//
//  DepartmentVC.swift
//  DrcPrectical
//
//  Created by Nipul Daki on 09/03/22.
//

import UIKit

class DepartmentVC: UIViewController {

    @IBOutlet weak var txtDepartment: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    // MARK: Button Action
    
    @IBAction func btnAction_Add(_ sender: Any) {
        saveData()
    }

    
    // MARK: Save Data Action
    
    func saveData() {
        guard let deptName = txtDepartment.text, deptName.count != 0 else {
           // Toast(text: Constants.ValidationMessage.enter_Title).show()
            return
        }
    }

}

