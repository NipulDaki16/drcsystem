import Foundation

extension String {
    
    func getStringToDate (formate : String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate
        let dateString = dateFormatter.date(from: self)
        return dateString!
    }

}
