

import Foundation
import UIKit

extension UITableView {
    func reloadDataWithAutoSizingCellWorkAround() {
        self.reloadData()
        self.setNeedsLayout()
        self.layoutIfNeeded()
        self.reloadData()
    }
    
    public func register(cellType: UITableViewCell.Type, bundle: Bundle? = nil) {
        let className = cellType.className
        let nib = UINib(nibName: className, bundle: bundle)
        register(nib, forCellReuseIdentifier: className)
    }
    
    public func register(cellTypes: [UITableViewCell.Type], bundle: Bundle? = nil) {
        cellTypes.forEach { register(cellType: $0, bundle: bundle) }
    }
    
    public func dequeueReusableCell<T: UITableViewCell>(with type: T.Type, for indexPath: IndexPath) -> T? {
        guard let cell = self.dequeueReusableCell(withIdentifier: type.className, for: indexPath) as? T else { return nil }
        return cell
    }
    
    func scroll(to: Position, animated: Bool) {
      let sections = numberOfSections
      let rows = numberOfRows(inSection: numberOfSections - 1)
      switch to {
      case .top:
        if rows > 0 {
          let indexPath = IndexPath(row: 0, section: 0)
          self.scrollToRow(at: indexPath, at: .top, animated: animated)
        }
        break
      case .bottom:
        if rows > 0 {
          let indexPath = IndexPath(row: rows - 1, section: sections - 1)
          self.scrollToRow(at: indexPath, at: .bottom, animated: animated)
        }
        break
      }
    }
    
    enum Position {
      case top
      case bottom
    }
}
