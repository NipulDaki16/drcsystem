//
//  EmpListCell.swift
//  DrcPrectical
//
//  Created by Nipul Daki on 09/03/22.
//

import UIKit

class EmpListCell: UITableViewCell {
    @IBOutlet weak var lblEmpName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
