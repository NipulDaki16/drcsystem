
import Foundation
import UIKit


struct Constants {
    
    //MARK:- Time Formate
    enum DateFormate:String {
        case yyyy_MM_dd_hh_mm = "yyyy-MM-dd HH:mm"
    }
    
    //MARK:- Message Validation
    struct ValidationMessage {
        static let enter_Title = "Please Enter Title"
        static let enter_Description = "Please Enter Description"
        static let select_Time = "Please Select Time"
        
    }
    
    
    //MARK:- AlertView Button Titel
    struct AlertText {
        static let Ok = "OK"
        static let Cancle = "Cancle"
    }
    
    struct AlertMessage {
        static let addSuccessMsg = "Alert Added Successfully!!!"
        static let editSuccessMsg = "Alert Edit Successfully!!!"
        static let deleteSuccessMsg = "Alert Deleted Successfully!!!"
    }
}



